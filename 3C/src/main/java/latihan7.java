
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class latihan7 extends JDialog {
private final JList list;
private final JPanel panel; 
private JLabel image;
private static final int FRAME_WIDTH = 600;
private static final int FRAME_HEIGHT = 300;
public latihan7 () {
 String[] names = {"Kanada","Jerman","Indonesia","Malaysia","Jepang","Spanyol","Vietnam","Italy"
                   ,"Portugal","Kameron","Inggris"};
  Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("ListDemo");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);  
        ImageIcon imagein = new ImageIcon("spanyol.png");
        image = new JLabel();
        image.setIcon(imagein);
        image.setBounds(110,50,200,100);
        contentPane.add(image);
        ImageIcon i = new ImageIcon("japan.png");
        image = new JLabel();
        image.setIcon(i);
        image.setBounds(120,20,200,60);
        contentPane.add(image);
        ImageIcon on = new ImageIcon("malay.png");
        image = new JLabel();
        image.setIcon(on);
        image.setBounds(240,20,200,60);
        contentPane.add(image);
        ImageIcon d = new ImageIcon("indo.png");
        image = new JLabel();
        image.setIcon(d);
        image.setBounds(240,60,200,100);
        contentPane.add(image);
        ImageIcon j = new ImageIcon("jerman.png");
        image = new JLabel();
        image.setIcon(j);
        image.setBounds(360,60,200,100);
        contentPane.add(image);
        ImageIcon ca = new ImageIcon("canada2.png");
        image = new JLabel();
        image.setIcon(ca);
        image.setBounds(360,20,200,60);
        contentPane.add(image);
        panel = new JPanel(new GridLayout(0, 1));
        list = new JList(names);
        panel.add(new JScrollPane(list));
        panel.setBounds(2,2,100,150);
        contentPane.add(panel);
}
  public static void main(String[] args) {
        latihan7 dialog = new latihan7();
        dialog.setVisible(true);
}
}
