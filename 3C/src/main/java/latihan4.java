
import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JTextArea;

public class latihan4 extends JDialog {

    private final JButton left;
    private final JButton right;
    private final JCheckBox cen, bon, it;
    private final JTextArea text;
    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGTH = 400;
    private static final int BUTTON_WIDTH = 90;
    private static final int BUTTON_HEIGTH = 40;

    public latihan4() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGTH);
        setResizable(true);
        setTitle("CheckBoxDemo");
        contentPane.setLayout(null);
        contentPane.setBackground(Color.WHITE);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        text = new JTextArea("Welcome to Java");
        text.setBounds(180, 100, 140, 40);
        contentPane.add(text);
        left = new JButton("Left");
        left.setBounds(160, 200, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(left);
        right = new JButton("Right");
        right.setBounds(260, 200, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(right);
        cen = new JCheckBox("Centreed");
        cen.setBounds(300, 500, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(cen);
        bon = new JCheckBox("Bold");
        bon.setBounds(300, 75, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(bon);
        it = new JCheckBox("Italic");
        it.setBounds(300, 100, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(it);
    }

    public static void main(String[] args) {
        latihan4 dialog = new latihan4();
        dialog.setVisible(true);
    }
}
