
import java.awt.Container;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;


public class latihan3 extends JDialog {
    private final JLabel image;
    private final JLabel text;
    private static final int FRAME_WIDTH = 100;
    private static final int FRAME_HEIGHT = 300;
    public latihan3(){
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Text and Icon Label");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        text = new JLabel("GRAPE");
        text.setBounds(35, 100, 97, 197);
        contentPane.add(text);
        ImageIcon imageic = new ImageIcon("grape.png");
        image = new JLabel();
        image.setIcon(imageic);
        image.setBounds(5, 10, 100, 100);
    }
    public static void main(String[] args) {
        latihan3 dialog = new latihan3();
        dialog.setVisible(true);
}
}